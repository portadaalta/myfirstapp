package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button boton;
    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton = (Button) findViewById(R.id.button);
        texto = (TextView) findViewById(R.id.textView);
        boton.setOnClickListener(this);
        //arreglar un error
        //actualizar();
    }

    private void actualizar() {

        String cadena = "123a";
        /*
        if (cadena.isEmpty()) {
            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        }
        else

        String cadena = "123a";
        try {
            int n = Integer.parseInt(cadena);
        } catch (NumberFormatException e)
        {
            texto.setText("Número mal");
        }
        */

        texto.setText(new Date().toString());
        Toast.makeText(this, "Fecha actualizada", Toast.LENGTH_LONG).show();
        Toast.makeText(this, "Mensaje posterior", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        if (view == boton) {
            actualizar();
        }
    }
}